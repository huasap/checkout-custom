<?php
/*
Plugin Name: Checkout-Custom
Plugin URI: https://gitlab.com/huasap/checkout-custom
Description: It is custom payment for Product Variation "Custom" for process Order
Author: Francisco Blanco
Version: 1.21.11.15.6
Author URI: https://franciscoblanco.vercel.app/
License: 
*/

define("CUSTOMCHECKOUTHUASAP","DEV");
define("CUSTOMCHECKOUTHUASAP_location_path", plugin_dir_path(__FILE__));
define("CUSTOMCHECKOUTHUASAP_location_url", plugin_dir_url(__FILE__));


require 'plugin-update-checker/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
	'https://gitlab.com/huasap/checkout-custom',
	__FILE__,
	'checkout-custom'
);
$myUpdateChecker->setAuthentication('MqiAAZzo9WNBAGzgwc3L');
$myUpdateChecker->setBranch(CUSTOMCHECKOUTHUASAP == "DEV" ? 'develop' : 'master');


require_once CUSTOMCHECKOUTHUASAP_location_path . 'src/_index.php';