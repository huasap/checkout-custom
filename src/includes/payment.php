<?php

function CUSTOMCHECKOUTHUASAP_add_Pay_Custom($methods)
{
    $methods[] = 'WC_Pay_Custom';
    return $methods;
}
add_filter('woocommerce_payment_gateways', 'CUSTOMCHECKOUTHUASAP_add_Pay_Custom');
function CUSTOMCHECKOUTHUASAP_woocommerce_Pay_Custom_gateway()
{
	if (!class_exists('WC_Payment_Gateway')) return;

	class WC_Pay_Custom extends WC_Payment_Gateway
	{
        /**
 		 * Class constructor, more about it in Step 3
 		 */
 		public function __construct() {

            $this->id = 'pay_custom'; // payment gateway plugin ID
            $this->method_title = 'Pay Custom';
	        $this->method_description = 'Pay what was agreed'; 
            
            $this->supports = array(
                'products'
            );
            
            $this->init_form_fields();
            
            $this->init_settings();
            
            $this->has_fields = false; // in case you need a custom credit card form
            $this->method_title = 'Pay Custom';
            $this->title = $this->get_option( 'title' ) == null ? 'Pay Custom' : $this->get_option( 'title' );
            $this->description = $this->get_option( 'description' ) == null ? 'Pay what was agreed' : $this->get_option( 'description' ); // will be displayed on the options page
	        $this->enabled = $this->get_option( 'enabled' );

            // This action hook saves the settings
            add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array( $this, 'process_admin_options' ) );
         
            // We need custom JavaScript to obtain a token
            //add_action( 'wp_enqueue_scripts', array( $this, 'payment_scripts' ) );
         
            // You can also register a webhook here
            // add_action( 'woocommerce_api_{webhook name}', array( $this, 'webhook' ) );
        }

		/**
		 * Funcion que define los campos que iran en el formulario en la configuracion
		 * de la pasarela de Pay_Custom
		 *
		 * @access public
		 * @return void
		 */
		function init_form_fields()
		{
            $this->form_fields = array(
                'enabled' => array(
                    'title' => __( 'Enable/Disable', 'woocommerce' ),
                    'type' => 'checkbox',
                    'label' => __( 'Enable', 'woocommerce' ),
                    'default' => isset($this->enabled)?$this->enabled:'yes'
                ),
                'title' => array(
                    'title' => __( 'Title', 'woocommerce' ),
                    'type' => 'text',
                    'description' => __( 'This controls the title which the user sees during checkout.', 'woocommerce' ),
                    'default' => isset($this->title)?$this->title:__( 'Pay Custom', 'woocommerce' ),
                    'desc_tip'      => true,
                ),
                'description' => array(
                    'title' => __( 'Customer Message', 'woocommerce' ),
                    'type' => 'textarea',
                    'default' => __("")
                )
            );
        }

        /*
        * Custom CSS and JS, in most cases required only when you decided to go with a custom credit card form
        */
        public function payment_scripts() {
            
        }
    
        /*
        * Fields validation, more in Step 5
        */
        public function validate_fields() {

        }
    
        /*
        * We're processing the payments here, everything about it is in Step 5
        */
        public function process_payment( $order_id ) {
            global $woocommerce;
            $order = new WC_Order( $order_id );
        
            // Mark as on-hold (we're awaiting the cheque)
            $order->update_status('processing', __( 'Pay Custom', 'woocommerce' ));
        
            // Remove cart
            $woocommerce->cart->empty_cart();
        
            // Return thankyou redirect
            return array(
                'result' => 'success',
                'redirect' => $this->get_return_url( $order )
            );
        }
    
        /*
        * In case you need a webhook, like PayPal IPN etc
        */
        public function webhook() {
        }
    }

}
add_action('plugins_loaded', 'CUSTOMCHECKOUTHUASAP_woocommerce_Pay_Custom_gateway', 0);